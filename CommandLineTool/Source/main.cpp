//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

template  <typename Type>
Type Min (Type a, Type b)
{
    if(a < b)
        return a;
    else
        return b;
}

template  <typename Type>
Type add (Type a, Type b)
{
    int add;
    
    add = (a + b);
    
    return add;
}

void print (int a[], int b)
{
    int i;

    for(i = 0; i < b; i++)
    {
        std::cout << "print count: " << a[i] << " \n";
    }
    
    
}

template  <typename Type>
Type getAverage(Type arr[], Type size) {
    int i, sum = 0;
    double avg;
    
    for (i = 0; i < size; ++i) {
        sum += arr[i];
    }
    avg = double(sum) / size;
    
    return avg;
}


int main()
{
    int array[] = {1,2,3,4,5};
    int bal[] = {1,2,3,4,5,6,7};
    
    std::cout << "min: " << Min( 567.23f, 32.45f) << " \n";
    std::cout << "add: " << add( 567.23f, 32.45f) << " \n";
    int sizeArray = sizeof(array)/sizeof(array[0]);
    print(array, sizeArray);
    
    int sizeBal = sizeof(bal)/sizeof(bal[0]);
    std::cout << "Average: " << getAverage(bal, sizeBal) << " \n";
    
    return 0;
}

